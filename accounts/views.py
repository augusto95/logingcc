from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.contrib.auth import login
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.urls import reverse
from django import forms

def home(request):
    return render(request, 'accounts/login.html')


def post(request):
    if User.objects.get(username__exact=request.user.username).is_superuser:
        return redirect('/menu/')
    else:
        return redirect('/menu/')

def user_logout(request):
        logout(request)
        return redirect('/accounts/login')
