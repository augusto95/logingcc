# loginGCC

<h3>Trabajo Práctico 2 GCC con Login</h3>

<li>
    Desarrollo: https://logingcc-dev.herokuapp.com
    <ul>
    Tareas: 
        <li>
            <strong>Verificacion</strong>: verifica que la app se haya desplegado correctamente.
        </li>
        <li>
            <strong>Desarrollo</strong>: despliega la rama "developer".
        </li>
    </ul>
</li>
<li>
    Testeo:
    <ul>
        Tareas:
        <li>
            <strong>Pruebas unitarias</strong>: ejecuta las pruebas unitarias en Django.
        </li>
    </ul>
</li>
<li>
    Producción: https://logingcc-prod.herokuapp.com
    <ul>
        Tareas:
        <li>
            <strong>Producción</strong>: despliega la rama "master".
        </li>
    </ul>
</li>
</ul>
Aplicación:
<ul>
    <li>
        <ul>Páginas:
            <li>Principal: una página que notifica al usuario que ha logrado ingresar.</li>
            <li>Iniciar Sesión: una página que solicita el usuario y la contraseña</li>
        </ul>
    </li>
    <li>
        <ul>Usuario:
            <li>augusto.</li>
            <li>Pass: augusto123</li>
        </ul>
    </li>

</ul>
