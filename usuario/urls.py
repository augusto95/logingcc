from django.conf.urls import include, url
from . import views

"""
Las url para la administración de usuarios
"""

urlpatterns = [
        url(r'^$', views.list_usuario),
]
