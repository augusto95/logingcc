from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.db import transaction
from django.db.models import Q
from django.shortcuts import render, redirect
from usuario.forms import UserForm, UsuarioForm, CreateUserForm, CambiarPasswordForm, BuscarUsername, BuscarCI
from .models import Usuario

"""
Las views para la administración de usuario
"""

@login_required
@transaction.atomic
def update_usuario(request, pk):
    """
    Esta view permite modificar (o eliminar) un User
    :param request:
    :param pk:
    """
    user = User.objects.get(pk=pk)
    if request.method == 'POST' and 'save' in request.POST:
        user_form = UserForm(request.POST,instance=user)
        usuario_form = UsuarioForm(request.POST ,instance=user.usuario)
        if user_form.is_valid() and usuario_form.is_valid():
            user_form.save()
            usuario_form.save()
            messages.success(request, ('Se actualizo el perfil seleccionado!'))
            return redirect('confirmado.html')
        else:
            messages.error(request, ('Corrija el error de abajo.'))
    elif request.method == 'POST' and 'eliminar' in request.POST:
        user_form = UserForm(request.POST, instance=user)
        usuario_form = UsuarioForm(request.POST, instance=user.usuario)
        user.delete()
        messages.success(request, ('Se elimino el perfil seleccionado!'))
        return redirect('confirmado.html')
    else:
        user_form = UserForm(instance=user)
        usuario_form = UsuarioForm(instance=user.usuario)
    return render(request, 'usuario/update_usuario.html', {
        'user_form': user_form,
        'usuario_form': usuario_form,
        'user':user
    })


@login_required
@transaction.atomic
def crear_usuario(request):
    """
    Esta view permite crear un User
    :param request:
    """
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            passw = form.cleaned_data['password']
            new_user = User.objects.create_user(**form.cleaned_data)
            messages.success(request, ('Se creó el perfil seleccionado!'))
            return redirect('confirmado.html')
    else:
        form = CreateUserForm()
    return render(request, 'usuario/create_usuario.html', {'form': form})


@login_required
@transaction.atomic
def list_usuario(request):
     """
     Esta view permite listar los User
     :param request:
     """
     users = User.objects.all()
     return render(request, 'usuario/list_usuario.html', {'users':users})

@login_required
@transaction.atomic
def cambiar_password(request):
    """
    Esta view permite a un User cambiar su propia contraseña
    :param request:
    """
    if request.method == "POST" and 'passw' in request.POST:
        form = CambiarPasswordForm(request.POST)
        if form.is_valid():
            u = User.objects.get(username__exact=request.user.username)
            passw = form.cleaned_data['password']
            u.set_password(passw)
            u.save()
            messages.success(request, ('Se cambió la contraseña!'))
            # redirect, or however you want to get to the main view
            return redirect('/usuario/confirmado_usuario')
    else:
        form = CambiarPasswordForm(request.POST)

    return render(request, 'usuario/cambiar_passw.html', {'form': form})

@login_required
@transaction.atomic
def buscar_username(request):
    """
    Buscar un usuario por username
    :param request:
    """

    if request.method == 'POST':
        busqueda = request.POST['buscalo']
        """
        Filtra de acuerdo a la palabra que se indico 
        :param request:
        """
        if busqueda:
            match = User.objects.filter(Q(username__contains=busqueda))
            if match:
                return render(request, 'usuario/buscar_username.html', {'sr': match})
            else:
                messages.error(request, 'No existe un usuario con ese nombre')
        else:
            return redirect("/usuario/buscar_username/")

    return render(request, 'usuario/buscar_username.html')


@login_required
@transaction.atomic
def buscar_ci(request):
    """
    Buscar un usuario por ci
    :param request:
    """

    if request.method == 'POST':
        busqueda = request.POST['buscalo']
        """
        Filtra de acuerdo a la palabra que se indico 
        :param request:
        """
        if busqueda:
            match = Usuario.objects.filter(Q(ci__contains=busqueda))
            if match:
                return render(request, 'usuario/buscar_ci.html', {'sr': match})
            else:
                messages.error(request, 'No existe un usuario con ese CI')
        else:
            matchh = Usuario.objects.filter(Q(ci__contains=busqueda))
            return render(request, 'usuario/buscar_ci.html', {'sr': matchh.all()})

    return render(request, 'usuario/buscar_ci.html')



@login_required
@transaction.atomic
def confirmado (request,pk):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    :param pk:
    """
    return render(request, 'usuario/confirmado.html')

@login_required
@transaction.atomic
def confirmado2 (request):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    """
    return render(request, 'usuario/confirmado.html')

@login_required
@transaction.atomic
def confirmado_usuario (request):
    """
    Página que confirma que un proceso fue realizado con éxito
    :param request:
    """
    return render(request, 'usuario/confirmado2.html')
