from django import forms
from django.contrib.auth.models import User
from django.forms import CharField, PasswordInput
from usuario.models import Usuario
import django.contrib.auth.forms
from django.forms import CharField, Form, PasswordInput

class UserForm(forms.ModelForm):
    """
    Formulario para la modificación de un User
    """
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

class UsuarioForm(forms.ModelForm):
    """
    Formulario para los campos de Usuario que está relacionado uno-a-uno con un User
    """
    class Meta:
        model = Usuario
        fields = ('ci', 'telefono', 'direccion', 'descripcion')

class CreateUserForm(forms.ModelForm):
    """
    Formulario para la creación de un nuevo User
    """
    class Meta:
        model = User
        fields = ('username', 'email', 'password')

class CambiarPasswordForm(Form):
        """
        Formulario para que un usuario pueda cambiar su propia contraseña
        """
        password = CharField(widget=PasswordInput())

class BuscarUsername(Form):
    """
    Formulario para buscar un user por username
    """
    username = CharField()

class BuscarCI(Form):
    """
    Formulario para buscar un user por CI
    """
    CI = CharField()