from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

"""
Definimos el modelo Usuario
"""

class Usuario(models.Model):
    """
    Definimos el modelo Usuario que está relacionado uno-a-uno con User.
    Definimos los campos que User no tiene predefinido pero está definido en la ERS del proyecto
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    ci = models.CharField(max_length=10, null=True, blank=True, unique=True)
    telefono = models.CharField(max_length=20, null=True, blank=True)
    direccion = models.CharField(max_length=40, null=True, blank=True)
    descripcion = models.TextField(null= True, blank=True)

    def __str__(self):
        return self.user.username

@receiver(post_save, sender=User)
def create_usuario(sender, instance, created, **kwargs):
      """
      Cuando se crea un User, además crea un objeto Usuario que está relacionado con el User
      :param sender:
      :param instance:
      :param created:
      :param kwargs:
      """
      if created:
          Usuario.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_usuario(sender, instance, **kwargs):
      """
      Cuando se guarda un User, los campos de su Usuario relacionado también se guardan.
      :param sender:
      :param instance:
      :param kwargs:
      """
      instance.usuario.save()
