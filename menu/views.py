from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.shortcuts import render
from django.contrib.auth.models import User

@login_required
@transaction.atomic
def menu(request):
    return render(request, 'menu/menu.html', { })


def index(request):
    return render(request, 'menu/index.html', { })

