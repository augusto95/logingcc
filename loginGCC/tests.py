import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "loginGCC.base")
django.setup()

from django.test import Client, TestCase
from django.contrib.auth.models import User


class Tests(TestCase):
    def test_pagina(self):
        client = Client()
        response = client.get("/accounts/login/")
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        user = User.objects.create(username='prueba_user')
        user.set_password('prueba_pass')
        user.save()
        client = Client()
        try:
            self.assertEqual(client.login(
                username='prueba_user', password='prueba_pass'), True)
        except AssertionError:
            raise

    def test_logout(self):
        client = Client()
        client.login(username='prueba_user', password='prueba_pass')
        response = client.get("/accounts/logout/")
        self.assertEqual(response.status_code, 302)
